#!/usr/bin/perl -w

my $format = 0;
my $c = 0;
my $b = 2;
while(<>) {
    if(/ABCD (\d+)\405*2\^\$a(.1) \[(\d+)\]/) {
	$format = 1;
	$k = $1;
	$c = $2;
	$n = $3;
	algCheck();
	next;
    }
    if(/^(\d+):([MP]):1:(\d+):25[78]/) {
	$format = 2;
	$b = $3;
	$c = ($2 eq 'M') ? -1 : 1;
	next;
    }
    if(/ABC \$a*(\d+)\^\$b(.1)/) {
	$format = 2;
	$b = $1;
	$c = $2;
	next;
    }
    if(/ABC \$a*2\^\$b\$c/) {
	$format = 3;
	next;
    }
    die "Unknown NewPgen file format!" if(/:/);
    if($format == 1 && /^(\d+)/) {
	$n += $1;
    } elsif($format == 2 && /^(\d+) (\d+)/) {
	$k = $1;
	$n = $2;
    } elsif($format == 3 && /^(\d+) (\d+) (\d+)/) {
	$k = $1;
	$n = $2;
	$c = $3;
    } else {
	die "Unknown data line";
    }
    algCheck();
}

sub algCheck {
    my $sc = ($c == 1) ? '+1' : '-1';
    my $x = int(sqrt($k+0.1));
    if($x*$x == $k && ($n%2) == 0) {
		if($c == -1) {
			print "$x*$b^",$n/2,"-1 | $k*$b^$n-1\n";
			return;
		} 
		# else c == 1: check for 4*X^4 + 1 = 4 * x^4 * (b^m)^4 + 1
		if($b == 2) {
			$x = int(exp(log($k+0.1)/4));
			if($k==$x**4 && ($n%4) == 2) { # probably only for k=625
				print $x*$x,"*2^",$n/2,"-$x*2^",($n+2)/4,"+1 | $k*2^$n+1\n";
				return;
			}
		} elsif (($x%2) == 0 && ($n%4) == 0) { # probably only for k=2500
			$x = int(sqrt($x/2+0.1));
			if($k==4*($x**4)) {
				print 2*$x*$x,"*$b^",$n/2,"-",2*$x,"*$b^",$n/4,"+1 | $k*$b^$n+1\n";
				return;
			}
		}
	}
    $x = int(exp(log($k+0.1)/3));
    if($x**3 == $k && ($n%3) == 0) {
	print "$x*$b^",$n/3,"$sc | $k*$b^$n$sc\n";
	return;
    }
    $x = int(exp(log($k+0.1)/5));
    if($x**5 == $k && ($n%5) == 0) {
	print "$x*$b^",$n/5,"$sc | $k*$b^$n$sc\n";
	return;
    }
    # for k>10000, use if($x**7 == $k && ($n%7) == 0), and possibly 11-th degree, too
    # for k<10000, we are now done with the last check:
    if($k == 2187 && ($n%7) == 0) {
	print "3*$b^",$n/7,"$sc | $k*$b^$n$sc\n";
    }
}
