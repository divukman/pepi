﻿namespace Pepi1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.mBtnFile = new System.Windows.Forms.Button();
            this.txtResults = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnSaveToFactorsTxt = new System.Windows.Forms.Button();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblCounter = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // mBtnFile
            // 
            this.mBtnFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mBtnFile.Location = new System.Drawing.Point(14, 14);
            this.mBtnFile.Name = "mBtnFile";
            this.mBtnFile.Size = new System.Drawing.Size(541, 58);
            this.mBtnFile.TabIndex = 0;
            this.mBtnFile.Text = "Select file";
            this.mBtnFile.UseVisualStyleBackColor = true;
            this.mBtnFile.Click += new System.EventHandler(this.mBtnFile_Click);
            // 
            // txtResults
            // 
            this.txtResults.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtResults.Location = new System.Drawing.Point(14, 78);
            this.txtResults.MaxLength = 120000;
            this.txtResults.Multiline = true;
            this.txtResults.Name = "txtResults";
            this.txtResults.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtResults.Size = new System.Drawing.Size(541, 468);
            this.txtResults.TabIndex = 1;
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(12, 600);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(541, 36);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Save results to algebraic.txt";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnSaveToFactorsTxt
            // 
            this.btnSaveToFactorsTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveToFactorsTxt.Location = new System.Drawing.Point(12, 643);
            this.btnSaveToFactorsTxt.Name = "btnSaveToFactorsTxt";
            this.btnSaveToFactorsTxt.Size = new System.Drawing.Size(541, 33);
            this.btnSaveToFactorsTxt.TabIndex = 3;
            this.btnSaveToFactorsTxt.Text = "Save results to factors.txt";
            this.btnSaveToFactorsTxt.UseVisualStyleBackColor = true;
            this.btnSaveToFactorsTxt.Click += new System.EventHandler(this.btnSaveToFactorsTxt_Click);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lblStatus.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.Location = new System.Drawing.Point(17, 561);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(150, 25);
            this.lblStatus.TabIndex = 4;
            this.lblStatus.Text = "Factors found:";
            // 
            // lblCounter
            // 
            this.lblCounter.AutoSize = true;
            this.lblCounter.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lblCounter.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.lblCounter.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCounter.Location = new System.Drawing.Point(199, 561);
            this.lblCounter.Name = "lblCounter";
            this.lblCounter.Size = new System.Drawing.Size(24, 25);
            this.lblCounter.TabIndex = 5;
            this.lblCounter.Text = "0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(567, 690);
            this.Controls.Add(this.lblCounter);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.btnSaveToFactorsTxt);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtResults);
            this.Controls.Add(this.mBtnFile);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Pepi 1: Perl to Win";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button mBtnFile;
        private System.Windows.Forms.TextBox txtResults;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnSaveToFactorsTxt;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label lblCounter;
    }
}

