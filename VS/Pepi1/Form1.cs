﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pepi1
{
    public partial class Form1 : Form
    {

        private static long counter = 0;        
        static public readonly string FACTORS_PREFIX = "3333333333333333333333";

        private StringBuilder mStrBldrResult = null;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void mBtnFile_Click(object sender, EventArgs e)
        {
            System.IO.Stream myStream = null;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = Application.StartupPath;
            openFileDialog1.RestoreDirectory = false;
            openFileDialog1.Filter = "Npg files (*.npg)|*.npg";
            openFileDialog1.FilterIndex = 2;        


            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = openFileDialog1.OpenFile()) != null)
                    {
                        using (myStream)
                        {
                            counter = 0;
                            mStrBldrResult = new StringBuilder();
                            lblCounter.Text = "";
                            txtResults.Clear();

                            // Insert code to read the stream here.      
                            StreamReader reader = new StreamReader(myStream);
                            string line;
                            long format = 0,
                                c = 0,
                                b = 2,
                                k = 0,
                                n = 0;

                            while ((line = reader.ReadLine()) != null)
                            {

                                //first condition
                                Regex regex = new Regex(@"ABCD (\d+)\405*2\^\$a(.1) \[(\d+)\]");
                                Match match = regex.Match(line);
                                if (match.Success)
                                {
                                    format = 1;
                                    k = Int64.Parse(match.Groups[1].Value);
                                    c = Int64.Parse(match.Groups[2].Value);
                                    n = Int64.Parse(match.Groups[3].Value);

                                    //MessageBox.Show("First condition: k = " + k + ", c = " + c + ", n = " + n);
                                    algCheck(c, k, n, b, txtResults, mStrBldrResult, lblCounter);
                                    continue;
                                }


                                //second condition
                                regex = new Regex(@"^(\d+):([MP]):1:(\d+):25[78]");
                                match = regex.Match(line);
                                if (match.Success)
                                {
                                    format = 2;
                                    b = Int64.Parse(match.Groups[3].Value);
                                    c = "M".Equals(match.Groups[2].Value) ? -1 : 1;
                                    //MessageBox.Show("Second condition: b = " + b + ", c = " + c);

                                    continue;
                                }


                                //third condition
                                regex = new Regex(@"ABC \$a*(\d+)\^\$b(.1)");
                                match = regex.Match(line);
                                if (match.Success)
                                {
                                    format = 2;
                                    b = Int64.Parse(match.Groups[1].Value);
                                    c = Int64.Parse(match.Groups[2].Value);
                                    //MessageBox.Show("Third condition: b = " + b + ", c = " + c);

                                    continue;
                                }


                                //fourth condition
                                regex = new Regex(@"ABC \$a*2\^\$b\$c");
                                match = regex.Match(line);
                                if (match.Success)
                                {
                                    format = 3;
                                    //MessageBox.Show("Fourth condition: format = 3");
                                    continue;
                                }


                                //exist in specific case
                                regex = new Regex(@":");
                                match = regex.Match(line);
                                if (match.Success)
                                {
                                    MessageBox.Show("Data not found! Exiting application!");
                                    txtResults.AppendText("Data not found!");
                                    Application.Exit();
                                }


                                //rest of the conditions
                                if (format == 1)
                                {
                                    regex = new Regex(@"^(\d+)");
                                    match = regex.Match(line);
                                    if (match.Success)
                                    {
                                        n += Int64.Parse(match.Groups[1].Value);
                                    }
                                }
                                else if (format == 2)
                                {
                                    regex = new Regex(@"^(\d+) (\d+)");
                                    match = regex.Match(line);
                                    if (match.Success)
                                    {
                                        k = Int64.Parse(match.Groups[1].Value);
                                        n = Int64.Parse(match.Groups[2].Value);
                                    }
                                }
                                else if (format == 3)
                                {
                                    regex = new Regex(@"^(\d+) (\d+) (\d+)");
                                    match = regex.Match(line);
                                    if (match.Success)
                                    {
                                        k = Int64.Parse(match.Groups[1].Value);
                                        n = Int64.Parse(match.Groups[2].Value);
                                        c = Int64.Parse(match.Groups[3].Value);
                                    }
                                }
                                else {
                                    //@todo log Unknown data line
                                    MessageBox.Show("Unknown data! Exiting application!");
                                    txtResults.AppendText("Unknown data!");
                                    Application.Exit();
                                }


                                algCheck(c, k, n, b, txtResults, mStrBldrResult,lblCounter);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }


        //@todo put results into string buffer
        private static void algCheck(long c, long k, long n, long b, TextBox txtBoxResults, StringBuilder strBldrResult, Label lblCounter) {
            string sc = c == 1 ? "+1" : "-1";
            long x = (long)(Math.Sqrt(k + 0.1));


            if (x * x == k && (n % 2 == 0)) {
                if (c == -1) {
                    string result1 = x +"*"+ b + "^" + n / 2 + "-1 |" + k + "*" + b + "^" + n + "-1"  + "\r\n";
                    string result2 = FACTORS_PREFIX + " | " + k + "*" + b + "^" + n + "-1" +  "\r\n";
                    strBldrResult.Append(result2);
                    txtBoxResults.AppendText(result1);
                    ++counter;
                    lblCounter.Text = counter.ToString();
                    return;
                }

                if (b == 2)
                {
                    x = (long)(Math.Exp(Math.Log(k + 0.1) / 4));
                    if (k == x * 4 && (n % 4) == 2)
                    {                        
                        string result1 = x * x + "*2^" + n / 2 + "-" + x + "*2^" + (n + 2) / 4 + "+1 | " + k + "*2^" + n + "+1" + "\r\n";
                        string result2 = FACTORS_PREFIX + " | " + k + "*2^" + n + "+1" + "\r\n";
                        strBldrResult.Append(result2);
                        txtBoxResults.AppendText(result1);
                        ++counter;
                        lblCounter.Text = counter.ToString();
                        return;
                    }
                }
                else if ((x % 2) == 0 && (n % 4) == 0) {
                    x = (long)Math.Sqrt(x / 2 + 0.1);
                    if (k == 4 * (Math.Pow(x, 4))) {
                        string result1 = 2 * x * x + "*" + b + "^" + n / 2 + "-" + 2 * x + "*" + b + "^" + n / 4 + "+1 | " + k + "*" + b + "^" + n + "+1" + "\r\n";                        
                        string result2 = FACTORS_PREFIX + " | " + k + "*" + b + "^" + n + "+1" + "\r\n";
                        strBldrResult.Append(result2);
                        txtBoxResults.AppendText(result1);
                        ++counter;
                        lblCounter.Text = counter.ToString();
                        return;
                    }
                }
            }


            x = (long)Math.Exp(Math.Log(k + 0.1) / 3);
            if (Math.Pow(x, 3) == k && (n % 3) == 0) {                
                string result1 = x + "*" + b + "^" + n / 3 + sc + " | " + k + "*" + b + "*" + "^" + n + sc + "\r\n";
                string result2 = FACTORS_PREFIX + " | " + k + "*" + b + "*" + "^" + n + sc + "\r\n";
                strBldrResult.Append(result2);
                txtBoxResults.AppendText(result1);
                ++counter;
                lblCounter.Text = counter.ToString();
                return;
            }

            x = (long)(Math.Exp(Math.Log(k + 0.1) / 5));
            if (Math.Pow(x, 5) == k && (n % 5) == 0) {                
                string result1 = x + "*" + b + "^" + n / 5 + sc + " | " + k + "*" + b + "*" + "^" + n + sc + "\r\n";
                string result2 = FACTORS_PREFIX + " | " + k + "*" + b + "*" + "^" + n + sc + "\r\n";
                strBldrResult.Append(result2);
                txtBoxResults.AppendText(result1);
                ++counter;
                lblCounter.Text = counter.ToString();
                return;
            }

            if (k == 2187 && (n%7) == 0)
            {
                // 
                string result1 = "3*" + b + "^" + n / 7 + sc + " | " + k + "*" + b + "*" + "^" + n + sc + "\r\n";
                string result2 = FACTORS_PREFIX + " | " + k + "*" + b + "*" + "^" + n + sc  + "\r\n";
                strBldrResult.Append(result2);
                txtBoxResults.AppendText(result1);
                ++counter;
                lblCounter.Text = counter.ToString();
                return;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (mStrBldrResult != null)
            {
                string fileName = Application.StartupPath + "\\algebraic.txt";
                File.WriteAllText(fileName, txtResults.Text);                
                MessageBox.Show("File saved!");
            }
            else {
                MessageBox.Show("Nothing to save!");
            }
            
        }

        private void btnSaveToFactorsTxt_Click(object sender, EventArgs e)
        {
            string fileName = Application.StartupPath + "\\factors.txt";
            File.WriteAllText(fileName, txtResults.Text);
            File.WriteAllText(fileName, mStrBldrResult.ToString());
            MessageBox.Show("File saved!");
        }
    }
}
